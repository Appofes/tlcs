/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tlcs;

import com.sun.pisces.Surface;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.TexturePaint;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RectangularShape;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ListIterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 *
 * @author Р”РјРёС‚СЂРёР№
 */
public abstract class GraphicsObject {
    //protected static final String DEFAULT_TEXTURE_PATH = "C:\\Users\\Дмитрий\\Documents\\NetBeansProjects\\TLCS\\images\\bmw-logo.jpg";    
    protected Point2D.Float m_position;
    protected int m_width;
    protected int m_height;
    protected BufferedImage m_texture;
    protected TexturePaint m_texturePaint;   
    
//    GraphicsObject(Point2D.Float position, int width, int height, String texture_path) {
//        m_position = position;
//        m_width = width;
//        m_height = height;
//        
//        try {
//            if(texture_path != null)
//            {
//                m_texture = ImageIO.read(new File(texture_path));
//            }
//            else
//            {
//                m_texture = ImageIO.read(new File(DEFAULT_TEXTURE_PATH));
//            }
//        }
//        catch(IOException e) {
//            Logger.getLogger(Surface.class.getName()).log(Level.SEVERE, null, e);
//        }
//        
//        m_texturePaint = new TexturePaint(m_texture, new Rectangle(m_width, m_height));
//    }
    
//    void draw(Graphics2D context) {
//        context.setPaint(m_texturePaint);
//        context.fillRect((int)m_position.x, (int)m_position.y, m_width, m_height);
//    }
//    
//    void update(float delta_time) {
//        /*update smth*/
//    }
    
    abstract void draw(Graphics2D context);
    abstract void update(float delta_time);
        
    Point2D.Float getPosition() {
        return new Point2D.Float(m_position.x, m_position.y);
    }
    
    void setPosition(Point2D.Float position) {
        if(0.0f <= position.x && 0.0f <= position.y) {
            m_position.x = position.x;
            m_position.y = position.y;
        }
    }
    
    public int getWidth() {
        return m_width;
    }
    
    public void setWidth(int width) {
        m_width = width;
    }
    
    public int getHeight() {
        return m_height;
    }
    
    public void setHeight(int height) {
        m_height = height;
    }
    
    public void moveBy(Point2D.Float offset) {
        m_position.x += offset.x;
        m_position.y += offset.y;
    }
    
    public void setTexture(String texture_file_name) {
        try {
            if(texture_file_name != null)
            {
                m_texture = ImageIO.read(new File(texture_file_name));
            }
        }
        catch(IOException e) {
            Logger.getLogger(Surface.class.getName()).log(Level.SEVERE, null, e);
        }
        
        int textureWidth = m_texture.getWidth();
        int textureHeight = m_texture.getHeight();
        
        m_texturePaint = new TexturePaint(m_texture, new Rectangle(/*(int)m_position.x, (int)m_position.y,*/ Math.abs(m_width), Math.abs(m_height)));
    }
}
