/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tlcs;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Point2D;

/**
 *
 * @author Дмитрий DO METHOD FINALIZE
 */
public class Zebra extends GraphicsObject {
    public static final String DEFAULT_ZEBRA_LANE_VERTICAL_TEXTURE_PATH = "C:\\Users\\Дмитрий\\Pictures\\zebra_lane_vertical.jpg";
    public static final String DEFAULT_ZEBRA_LANE_HORIZONTAL_TEXTURE_PATH = "C:\\Users\\Дмитрий\\Pictures\\zebra_lane_horizontal.jpg";
    private Point2D.Float m_firstOrigin;
    private Point2D.Float m_secondOrigin;
    private boolean m_isVertical;
    
    private Lane m_firstLane;
    private Lane m_secondLane;
    
    public Zebra(Point2D.Float first_origin, Point2D.Float second_origin, boolean is_vertical) {
        if(is_vertical)
        {
//            m_firstLane = new Lane(first_origin, UI.DEFAULT_LANE_WIDTH, -UI.DEFAULT_LANE_HEIGHT, true);
//            m_secondLane = new Lane(second_origin, UI.DEFAULT_LANE_WIDTH, UI.DEFAULT_LANE_HEIGHT, true);
            m_firstLane = new Lane(first_origin, UI.DEFAULT_LANE_WIDTH, -UI.DEFAULT_LANE_HEIGHT / 2, true);
            m_firstLane.setTexture(DEFAULT_ZEBRA_LANE_VERTICAL_TEXTURE_PATH);
            m_secondLane = new Lane(second_origin, UI.DEFAULT_LANE_WIDTH, UI.DEFAULT_LANE_HEIGHT / 2, true);
            m_secondLane.setTexture(DEFAULT_ZEBRA_LANE_VERTICAL_TEXTURE_PATH);
            
            m_width = UI.DEFAULT_LANE_WIDTH;
            m_height = (int)(second_origin.y - first_origin.y);
            m_position = new Point2D.Float(first_origin.x, first_origin.y);
        }
        else
        {
//            m_firstLane = new Lane(first_origin, -UI.DEFAULT_LANE_HEIGHT, UI.DEFAULT_LANE_WIDTH, true);
//            m_secondLane = new Lane(second_origin, UI.DEFAULT_LANE_HEIGHT, UI.DEFAULT_LANE_WIDTH, true);
            
            m_firstLane = new Lane(first_origin, -UI.DEFAULT_LANE_HEIGHT / 2, UI.DEFAULT_LANE_WIDTH, true);
            m_firstLane.setTexture(DEFAULT_ZEBRA_LANE_HORIZONTAL_TEXTURE_PATH);
            m_secondLane = new Lane(second_origin, UI.DEFAULT_LANE_HEIGHT / 2, UI.DEFAULT_LANE_WIDTH, true);
            m_secondLane.setTexture(DEFAULT_ZEBRA_LANE_HORIZONTAL_TEXTURE_PATH);
            
            m_width = (int)(second_origin.x - first_origin.x);
            m_height = UI.DEFAULT_LANE_WIDTH;
            m_position = new Point2D.Float(first_origin.x, first_origin.y);
        }
        
        m_firstLane.setGreenLight(new PedestrianGreenLight());
        m_secondLane.setGreenLight(new PedestrianGreenLight());
    }

    @Override
    void draw(Graphics2D context) {
        m_firstLane.draw(context);
        m_secondLane.draw(context);
        
        context.setColor(Color.RED);
        //fillRect, not drawRect
        context.drawRect((int)m_position.x, (int)m_position.y, m_width, m_height);
    }

    @Override
    void update(float delta_time) {
        
    }
    
    public void setState(Color state) {
        m_firstLane.setGreenLightState(new Color(state.getRGB()));
        m_secondLane.setGreenLightState(new Color(state.getRGB()));
    }
    
    public Lane getFocusedLane(int x, int y) {
        if(m_firstLane.contains(x, y))
        {
            return m_firstLane;
        }
        else if(m_secondLane.contains(x, y))
        {
            return m_secondLane;
        }
        else
        {
            return null;
        }
    }
    
    public void moveAllBy(Point2D.Float offset) {
        this.moveBy(offset);
        m_firstLane.moveBy(offset);
        m_secondLane.moveBy(offset);
    }
    
    public void moveSecondLaneBy(Point2D.Float offset) {
        m_secondLane.moveBy(offset);
        m_width += offset.x;
        m_height += offset.y;
    }
            
    
}
