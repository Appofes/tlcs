/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tlcs;

import java.awt.geom.Point2D;

/**
 *
 * @author Дмитрий
 */
public class Trajectory {
    private Point2D.Float m_controlPoints[];
    
    public Trajectory(Point2D.Float control_point0, Point2D.Float control_point1, Point2D.Float control_point2) {
        m_controlPoints = new Point2D.Float[3];
        
        m_controlPoints[0] = new Point2D.Float(control_point0.x, control_point0.y);
        m_controlPoints[1] = new Point2D.Float(control_point1.x, control_point1.y);
        m_controlPoints[2] = new Point2D.Float(control_point2.x, control_point2.x);
    }
    
    public Point2D.Float getPosition(float t) {
        float factor1 = (1 - t) * (1 - t);
        float factor2 = 2 * t * (1 - t);
        float factor3 = t * t;
        
        float x = m_controlPoints[0].x * factor1 + m_controlPoints[1].x * factor2 + m_controlPoints[2].x * factor3;
        float y = m_controlPoints[0].y * factor1 + m_controlPoints[1].y * factor2 + m_controlPoints[2].y * factor3;
        
        return new Point2D.Float(x, y);
    }
}
