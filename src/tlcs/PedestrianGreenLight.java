/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tlcs;

import java.awt.Color;
import java.awt.geom.Point2D;

/**
 *
 * @author Р”РјРёС‚СЂРёР№
 */
public class PedestrianGreenLight extends GreenLight {

    public PedestrianGreenLight(Point2D.Float position, int size, int delay, Color initial_state) {
        super(position, size, delay, initial_state);
    }
    
    public PedestrianGreenLight() {
        super(new Point2D.Float(0, 0), 0, 0, Color.RED);
    }
    
    @Override
    public void setState(Color state) {
        if(state.equals(Color.RED) || state.equals(Color.GREEN))
        {
            m_currentState = state;
        }
    }
    
}
