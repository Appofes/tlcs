/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tlcs;

import java.awt.geom.Point2D;

/**
 *
 * @author Дмитрий
 */
public class Target {
    private boolean m_engaged;
    private boolean m_isReceiver;
    private Point2D.Float m_position;
    
    public Target(Point2D.Float position, boolean is_receiver) {
        m_position = new Point2D.Float(position.x, position.y);
        m_engaged = false;
        m_isReceiver = is_receiver;
    }
    
    public boolean isReceiver() {
        return m_isReceiver;
    }
    
    public boolean isEngaged() {
        return m_engaged;
    }
    
    public boolean enter() {
        if(m_engaged)
        {
            return false;
        }
        else
        {
            m_engaged = true;
            return true;
        }
    }
    
    public void leave() {
        m_engaged = false;
    }
    
}
