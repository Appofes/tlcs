/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tlcs;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 *
 * @author Дмитрий
 */
public class HitDetector extends MouseAdapter {
    private Road m_roads[];
    private Lane m_focusedLane;
    
    public HitDetector(Road roads[]) {
        m_roads = roads;
        m_focusedLane = null;
    }
    
    @Override
    public void mousePressed(MouseEvent mouse_event) {
        int x = mouse_event.getX();
        int y = mouse_event.getY();
        
        //this.unfocusAll();
        
        m_focusedLane = null;
        
        for(Road road : m_roads)
        {
            //if(road.getBounds().contains(x, y))
            //{
            //  road.focusLane(x, y);  
            //}
            if(road != null)
            {
                Lane focusedLane = road.getFocusedLane(x, y);

                if(focusedLane != null)
                {
                    m_focusedLane = focusedLane;
                    break;
                }
            }
        }
        
        System.out.println(x + " " + y);
    }
    
    public Lane getFocusedLane() {
        return m_focusedLane;
    }
    
//    private void unfocusAll() {
//        for(Road road : m_roads) 
//        {
//            //road.unfocusAllLanes();
//        }
//    }
    
}
