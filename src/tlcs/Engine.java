/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tlcs;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Point2D;
import static java.lang.System.exit;
import javax.swing.Timer;

enum CROSSBAR {VERTICAL, HORIZONTAL}

/**
 *
 * @author Р”РјРёС‚СЂРёР№
 */
public class Engine extends javax.swing.JPanel implements ActionListener {
    private final float DEFAULT_GREEN_DELAY = 1000;
    private final float DEFAULT_YELLOW_DELAY = 500;
    private final float DEFAULT_RED_DELAY = 1000;
    private final int DEFAULT_TIMER_DELAY = 16;
    
    private float m_greenDelay;
    private float m_yellowDelay;
    private float m_redDelay;
    
    private Road m_roads[];
    private CROSSBAR m_activeCrossbar;
    
    private long m_cachedTime;
    
    private Timer m_timer;
    
    private float m_accumulator;
    
    private SWITCH_DIRECTION m_switchDirection;
    
    private HitDetector m_hitDetector;
    
    /**
     * Creates new form Engine
     */
    public Engine() {
        this.initComponents();
        
        m_timer = new Timer(DEFAULT_TIMER_DELAY, this);
        
        m_accumulator = 0.0f;
        
        m_roads = new Road[4];
        
        for(Road road : m_roads)
        {
            road = null;
        }
        
        m_activeCrossbar = CROSSBAR.VERTICAL;
        
        m_switchDirection = SWITCH_DIRECTION.DOWN;
        
        m_greenDelay = DEFAULT_GREEN_DELAY;
        m_yellowDelay = DEFAULT_YELLOW_DELAY;
        m_redDelay = DEFAULT_RED_DELAY;
        
        //this.addMouseListener(new HitDetector(m_roads));
        
        m_hitDetector = new HitDetector(m_roads);
        addMouseListener(m_hitDetector);
    }
    
    public void launch() {
        //m_hitDetector = new HitDetector(m_roads);
        //addMouseListener(m_hitDetector);
        ////addMouseListener(new HitDetector(m_roads));
        
        m_cachedTime = System.currentTimeMillis();
        m_timer.start();
    }
    
    public void addTopRoad(Road top_road)
    {
        m_roads[0] = top_road;
    }
    
    public void addBottomRoad(Road bottom_road)
    {
        m_roads[1] = bottom_road;
    }
    
    public void addLeftRoad(Road left_road)
    {
        m_roads[2] = left_road;
    }
    
    public void addRightRoad(Road right_road)
    {
        m_roads[3] = right_road;
    }
    
    @Override
    public void paintComponent(Graphics context) {
        super.paintComponent(context);
        this.draw((Graphics2D)context);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        long delta_time = System.currentTimeMillis() - m_cachedTime;
        m_cachedTime = System.currentTimeMillis();
        
        this.update(delta_time);
        this.draw();
    }

    
    synchronized public void update(long delta_time) {
        /*do some update work*/
        
        m_accumulator += (float)delta_time;
        
        if(getCrossbarState(m_activeCrossbar).equals(Color.GREEN) && m_greenDelay <= m_accumulator)
        {
            setCrossbarState(m_activeCrossbar, Color.YELLOW);
            m_switchDirection = SWITCH_DIRECTION.UP;
            m_accumulator = 0.0f;                      
        }
        else if(getCrossbarState(m_activeCrossbar).equals( Color.YELLOW) && m_yellowDelay <= m_accumulator)
        {
           Color nextState = (m_switchDirection == SWITCH_DIRECTION.UP) ? Color.RED : Color.GREEN;
           
           setCrossbarState(m_activeCrossbar, nextState);
           
           if(m_switchDirection == SWITCH_DIRECTION.UP)
           {
                switchCrossbar();
           }
           
           m_accumulator = 0.0f;
        }
        else if(getCrossbarState(m_activeCrossbar).equals(Color.RED) && m_redDelay <= m_accumulator)
        {
           setCrossbarState(m_activeCrossbar, Color.YELLOW);
           m_switchDirection = SWITCH_DIRECTION.DOWN;
           m_accumulator = 0.0f;
        }
    }
    
    synchronized public void draw() {
        repaint();
    }
    
    synchronized private void draw(Graphics2D context) {
        /*do some draw work*/        
        
        for(Road road : m_roads)
        {
            if(road != null)
            {
                road.draw(context);
            }
        }
    }
    
    private Color getCrossbarState(CROSSBAR crossbar) {
        switch(crossbar)
        {
            case VERTICAL:
                if(m_roads[0] != null & !m_roads[0].isEmpty())
                {
                    return m_roads[0].getState();
                }
                else if(m_roads[1] != null & !m_roads[1].isEmpty())
                {
                    return m_roads[1].getState();
                }
                break;
                
            case HORIZONTAL:
                if(m_roads[2] != null & !m_roads[2].isEmpty())
                {
                    return m_roads[2].getState();
                }
                else if(m_roads[3] != null & !m_roads[3].isEmpty())
                {
                    return m_roads[3].getState();
                }
                break;              
        }
        
        exit(0);
        
        return Color.RED;
    }
    
    private void setCrossbarState(CROSSBAR crossbar, Color state)
    {
        switch(crossbar)
        {
            case VERTICAL:
                if(m_roads[0] != null & !m_roads[0].isEmpty())
                {
                    m_roads[0].setState(new Color(state.getRGB()));                    
                }
                if(m_roads[1] != null & !m_roads[1].isEmpty())
                {
                    m_roads[1].setState(new Color(state.getRGB()));
                }
                if(m_roads[2] != null)
                {
                    m_roads[2].setZebraState(new Color(state.getRGB()));
                }
                if(m_roads[3] != null)
                {
                    m_roads[3].setZebraState(new Color(state.getRGB()));
                }
                break;
                
            case HORIZONTAL:
                if(m_roads[2] != null & !m_roads[2].isEmpty())
                {
                    m_roads[2].setState(new Color(state.getRGB()));
                }
                if(m_roads[3] != null & !m_roads[3].isEmpty())
                {
                    m_roads[3].setState(new Color(state.getRGB()));
                }
                if(m_roads[0] != null)
                {
                    m_roads[0].setZebraState(new Color(state.getRGB()));
                }
                if(m_roads[1] != null)
                {
                    m_roads[1].setZebraState(new Color(state.getRGB()));
                }
                break;              
        }
    }
    
    public void switchCrossbar() {
        if(m_activeCrossbar == CROSSBAR.VERTICAL)
        {
            m_activeCrossbar = CROSSBAR.HORIZONTAL;
        }
        else
        {
            m_activeCrossbar = CROSSBAR.VERTICAL;
        }
    }
    
    
    
    public void setGreenDelay(float delay) {
        if(0 < delay) {
            m_greenDelay = delay;
        }
        //m_greenDelay = delay;
    }
    
    public void setYellowDelay(float delay) {
        if(0 < delay) {
            m_yellowDelay = delay;
        }
        //m_yellowDelay = delay;
    }
    
    public void setRedDelay(float delay) {
        if(0 < delay) {
            m_redDelay = delay;
        }
        //m_redDelay = delay;
    }
    
    public Lane getFocusedLane() {
        return m_hitDetector.getFocusedLane();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}


