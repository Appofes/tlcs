/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tlcs;

import com.sun.pisces.Surface;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.TexturePaint;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 *
 * @author Р”РјРёС‚СЂРёР№
 */
public abstract class TrafficParticipant extends GraphicsObject {
    //protected Point2D.Float m_velocity;
    protected Trajectory m_trajectory;
    protected float m_trajectoryArg;
    protected Color m_color;
    //protected BufferedImage m_texture;
    //protected TexturePaint m_texturePaint;
    
//    public TrafficParticipant(Point2D.Float position, int width, int height, Color color) {
//        m_position = new Point2D.Float(position.x, position.y);
//        m_width = width;
//        m_height = height;
//        m_color = color;
//        m_trajectoryArg = 0.0f;
//        m_trajectory = null;
//    }
    
//    public TrafficParticipant(Point2D.Float position, int size, Color color) {
//        m_position = position;
//        m_width = size;
//        m_height = size;
//        m_color = Color.BLACK;
//        m_trajectoryArg = 0.0f;
//        m_trajectory = null;
//    }
    
//    public TrafficParticipant() {
//        m_position = new Point2D.Float(0, 0);
//        m_width = UI.DEFAULT_TRAFFIC_PARTICIPANT_SIZE;
//        m_height = UI.DEFAULT_TRAFFIC_PARTICIPANT_SIZE;
//        m_color = Color.BLACK;
//        m_trajectoryArg = 0.0f;
//        m_trajectory = null;
//    }
            
//    public TrafficParticipant(Point2D.Float position, int width, int height, String texture_path, Point2D.Float velocity) {
//        m_position = new Point2D.Float(position.x, position.y);
//        
//        m_width = width;
//        m_height = height;
//        
//        try {
//            if(texture_path != null)
//            {
//                m_texture = ImageIO.read(new File(texture_path));
//            }
//            else
//            {
//                m_texture = ImageIO.read(new File(DEFAULT_TEXTURE_PATH));
//            }
//        }
//        catch(IOException e) {
//            Logger.getLogger(Surface.class.getName()).log(Level.SEVERE, null, e);
//        }
//        
//        m_texturePaint = new TexturePaint(m_texture, new Rectangle(m_width, m_height));
//        
//        this.setVelocity(velocity);
//        
//        m_trajectory = null;
//        m_trajectoryArg = 0.0f;
//    }
    
//    public void setVelocity(Point2D.Float velocity) {
//        m_velocity.x = velocity.x;
//        m_velocity.y = velocity.y;
//    }
    
//    public Point2D.Float getVelocity() {
//        return new Point2D.Float(m_velocity.x, m_velocity.y);
//    }
    
    @Override
    void update(float delta_time) {
        //m_position.x += m_velocity.x * delta_time;
        //m_position.y += m_velocity.y * delta_time;
        
        if(m_trajectory != null)
        {
            m_position.x = m_trajectory.getPosition(m_trajectoryArg).x;
            m_position.y = m_trajectory.getPosition(m_trajectoryArg).y;
            //m_trajectoryArg += OFFSET;
        }
        else
        {
            m_trajectoryArg = 0.0f;
        }
    }
    
    @Override
    void draw(Graphics2D context) {
        /*do some draw here*/
        
//        context.setPaint(m_texturePaint);
//        context.fillRect((int)m_position.x, (int)m_position.y, m_width, m_height);
        if(m_texture == null)
        {
            context.setColor(m_color);
            context.fillRect((int)m_position.x, (int)m_position.y, m_width, m_height);
        }
        else
        {
            //context.setPaint(m_texturePaint);
            context.drawImage(m_texture, null, (int)m_position.x, (int)m_position.y);
        }
        
    }
}
