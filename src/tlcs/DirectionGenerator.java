/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tlcs;

import static java.lang.System.exit;

/**
 *
 * @author Дмитрий
 */

public class DirectionGenerator {
    public enum DIRECTION {FORWARD, LEFT, RIGHT}
    
    private DirectionGenerator() {
        
    }
    
    public static DIRECTION next() {
        switch((int)(Math.random() * 3))
        {
            case 0:
                return DIRECTION.FORWARD;
                
            case 1:
                return DIRECTION.LEFT;
                
            case 2:
                return DIRECTION.RIGHT;
                
            default:
                exit(-1);
                return DIRECTION.FORWARD;
        }
    }
}
