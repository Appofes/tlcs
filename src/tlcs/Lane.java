/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tlcs;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

enum ORIENTATION {UP, DOWN, LEFT, RIGHT }

/**
 *
 * @author Дмитрий
 */
public class Lane extends GraphicsObject {
    //private static final int DEFAULT_HEIGHT = 100;
    //private static final int DEFAULT_WIDTH = -200;
    
    public static final String DEFAULT_VERTICAL_LANE_TEXTURE_PATH = "C:\\Users\\Дмитрий\\Pictures\\road_vertical.jpg";
    public static final String DEFAULT_HORIZONTAL_LANE_TEXTURE_PATH = "C:\\Users\\Дмитрий\\Pictures\\road_horizontal.jpg";
    
    protected int DEFAULT_QUEUE_SIZE = 5;
    
    protected boolean m_toCrossDir;//можно заменить на boolean toCrossDir
    protected ArrayList<TrafficParticipant> m_queue;//Сделать Queue
    protected int m_maxParticipantsInQueue;
    protected GreenLight m_greenLight;
    
    protected ArrayList<Target> m_leftTargets;
    protected ArrayList<Target> m_frontTargets;
    protected ArrayList<Target> m_rightTargets;
    protected ArrayList<Target> m_bottomTargets;
    
    protected Color m_color;
    
    //protected Point2D.Float m_startQueuePosition;
    //protected boolean m_focused;
    ///protected Point2D.Float m_pushOffset;
    
    public Lane(Point2D.Float position, int width, int height, boolean to_cross_dir) {
        m_position = new Point2D.Float(position.x, position.y);
        
        m_width = width;
        m_height = height;
        
        m_toCrossDir = to_cross_dir;
        
        this.configGreenLight(null);
        
        m_frontTargets = new ArrayList<Target>();
        m_rightTargets = new ArrayList<Target>();
        m_leftTargets = new ArrayList<Target>();
        m_bottomTargets = new ArrayList<Target>();
        
        m_color = (to_cross_dir) ? Color.WHITE : Color.GRAY;
        
        m_queue = new ArrayList<>();
        
//        m_maxParticipantsInQueue = (Math.abs(m_width) < Math.abs(m_height)) ? 
//                Math.abs(m_height) / UI.DEFAULT_TRAFFIC_PARTICIPANT_HEIGHT : 
//                Math.abs(m_width) / UI.DEFAULT_TRAFFIC_PARTICIPANT_HEIGHT;
        
        
        ///m_pushOffset = new Point2D.Float();
        
        //m_focused = false;
        
        //m_startQueuePosition = new Point2D.Float();
        //this.configStartQueuePosition();
        
        //this.setTexture(DEFAULT_LANE_TEXTURE_PATH);
        
        switch(this.determineOrientation())
        {
            case UP:
                this.setTexture(DEFAULT_VERTICAL_LANE_TEXTURE_PATH);
                break;
                
            case DOWN:
                this.setTexture(DEFAULT_VERTICAL_LANE_TEXTURE_PATH);
                break;
                
            case LEFT:
                this.setTexture(DEFAULT_HORIZONTAL_LANE_TEXTURE_PATH);
                break;
                
            case RIGHT:
                this.setTexture(DEFAULT_HORIZONTAL_LANE_TEXTURE_PATH);
                break;
        }
    }
    
    private void configGreenLight(GreenLight green_light) {
        int size = (Math.abs(m_width) < Math.abs(m_height)) ? Math.abs(m_width) : Math.abs(m_height);
        Point2D.Float position = new Point2D.Float(m_position.x, m_position.y);
        
        ///m_pushOffset = new Point2D.Float()
        
        if(0 < m_width && m_height < 0)
        {
            position.y -= size;
        }
        if(m_width < 0 && 0 < m_height)
        {
            position.x -= size;
        }
        if(m_width < 0 && m_height < 0)
        {
            position.x -= size;
            position.y -= size;
        }
        
        if(green_light == null)
        {
            m_greenLight = new GreenLight(position, size, 0, Color.RED);
        }
        else
        {
            green_light.setPosition(position);
            green_light.setHeight(size);
            green_light.setWidth(size);
            green_light.setState(Color.RED);
            
            m_greenLight = green_light;
        }
    }
    
//    public void configStartQueuePosition() {
//        switch(this.determineOrientation())
//        {
//            case UP:
//                m_startQueuePosition.x = m_position.x;
//                m_startQueuePosition.y = m_position.y - m_greenLight.getHeight() * 2;
//                break;
//                
//            case DOWN:
//                m_startQueuePosition.x = m_position.x;
//                m_startQueuePosition.y = m_position.y + m_greenLight.getHeight();
//                break;
//                
//            case LEFT:
//                m_startQueuePosition.x = m_position.x - m_greenLight.getWidth() * 2;
//                m_startQueuePosition.y = m_position.y;
//                break;
//                
//            case RIGHT:
//                m_startQueuePosition.x = m_position.x + m_greenLight.getWidth();
//                m_startQueuePosition.y = m_position.y;
//                break;
//        }
//    }
       
    
//    public void switchLight() {
//        m_greenLight.switchLight();
//    }
    
    /*Realization-depended method*/
    public ORIENTATION determineOrientation() {
        if(m_width < 0)
        {
            return ORIENTATION.LEFT;
        }
        else if(m_height < 0)
        {
            return ORIENTATION.UP;
        }
        else if(m_width < m_height)
        {
            return ORIENTATION.DOWN;
        }
        else
        {
            return ORIENTATION.RIGHT;
        }
    }
    
    public Color getGreenLightState() {
        return m_greenLight.getCurrentState();
    }
    
    @Override
    void draw(Graphics2D context) {
        if(m_texture == null)
        {
            context.setColor(m_color);
            context.fillRect((int)m_position.x, (int)m_position.y, m_width, m_height);
        }
        else 
        {
            Point2D.Float offset = new Point2D.Float(0, 0);
            
            switch(this.determineOrientation())
            {
                case UP:
                    offset.y = m_height;
                    break;
                    
                case DOWN:
                    //offset.y = m_height;
                    break;
                    
                case LEFT:
                    offset.x = m_width;
                    break;
                    
                case RIGHT:
                    //offset.x = m_width;
                    break;
            }
            context.drawImage(m_texture, null, (int)(m_position.x + offset.x), (int)(m_position.y + offset.y));
        }
        
        //context.setPaint(m_texturePaint);
        //context.fillRect((int)m_position.x, (int)m_position.y, m_width, m_height);
        
        m_greenLight.draw(context);
        
        for(TrafficParticipant trafficParticipant : m_queue)
        {
            if(trafficParticipant != null)
            {
                trafficParticipant.draw(context);
            }
        }
    }

    @Override
    void update(float delta_time) {
        /*DirectionGenerator.DIRECTION direction = DirectionGenerator.next();
        Target target = this.chooseTarget(direction);
        
        if(target != null)
        {
            switch(direction)
            {
                case LEFT:
                    while(!target.enter())
                    {
                        //INTENTIONALLY EMPTY
                    }
                    if(m_greenLight.getCurrentState().equals(Color.GREEN) && isOppositeLaneFree())
                    {
                        //MOVE
                        //WAIT
                        //delete from queue
                    }
                    else
                    {
                        target.leave();
                    }
                    break;
                    
                default:
                    while(!target.enter())
                    {
                        //INTENTIONALLY_EMPTY
                    }
                    if(m_greenLight.getCurrentState().equals(Color.GREEN))
                    {
                        //move
                    }
                    else
                    {
                        target.leave();
                    }
                    break;
            }
        }
        Thread thread = new Thread();
        //UPDATE ALL*/
        
        m_greenLight.update(delta_time);
    }
    
    boolean isOppositeLaneFree() {
        boolean isFree = true;
        
        for(int i = 0; i < m_bottomTargets.size(); i++)
        {
            if(m_bottomTargets.get(i).isEngaged())
            {
                isFree = false;
                break;
            }
        }
        
        return isFree;
    }
    
    Target chooseTarget(DirectionGenerator.DIRECTION direction) {
        switch(direction)
        {
            case FORWARD:
                if(!m_frontTargets.isEmpty())
                {
                    return m_frontTargets.get((int)(Math.random() * m_frontTargets.size()));
                }
                break;

            case LEFT:
                if(!m_leftTargets.isEmpty())
                {
                    return m_leftTargets.get((int)(Math.random() * m_leftTargets.size()));
                }
                break;

            case RIGHT:
                if(!m_rightTargets.isEmpty())
                {
                    return m_rightTargets.get((int)(Math.random() * m_rightTargets.size()));
                }
                break;
        }
       
        return null;
    }
    
    void setGreenLight(GreenLight green_light) {
        //m_greenLight = green_light;//
        this.configGreenLight(green_light);
    }
    
    /*void addParticipant(TrafficParticipant traffic_participant) {
        if(m_queue.size() < m_maxParticipantsInQueue)
        {
            m_queue.add(traffic_participant);
        }
    }*/
    
    public void pushParticipant(TrafficParticipant participant) {
        int size = (Math.abs(m_width) < Math.abs(m_height)) ? Math.abs(m_width) : Math.abs(m_height);
        
        Point2D.Float position = new Point2D.Float(m_position.x, m_position.y);
        
        participant.setWidth(size);
        participant.setHeight(size);
        
        switch(this.determineOrientation())
        {
            case UP:
                position.y -= m_greenLight.getHeight() + size * (m_queue.size() + 1);
                if(m_position.y + m_height <= position.y)
                {
                    participant.setPosition(position);                    
                    m_queue.add(participant);
                }
                break;
                
            case DOWN:
                position.y += m_greenLight.getHeight() + size * m_queue.size();
                if(position.y <= m_position.y + m_height)
                {
                    participant.setPosition(position);
                    m_queue.add(participant);
                }
                break;
                
            case LEFT:
                position.x -= m_greenLight.getWidth() + size * (m_queue.size() + 1);
                if(m_position.x + m_width <= position.x)
                {
                    participant.setPosition(position);
                    m_queue.add(participant);
                }
                break;
                
            case RIGHT:
                position.x += m_greenLight.getWidth() + size * (m_queue.size());
                if(position.x <= m_position.x + m_width)
                {
                    participant.setPosition(position);
                    m_queue.add(participant);
                }
        }
    }
    
    public void popParticipant() {
        if(!m_queue.isEmpty())
        {
            m_queue.remove(m_queue.size() - 1);
        }
    }
        
    void addFrontTargets(ArrayList<Target> targets) {
        m_frontTargets.addAll(targets);
    }
    
    void addRightTargets(ArrayList<Target> targets) {
        m_rightTargets.addAll(targets);
    }
    
    void addLeftTargets(ArrayList<Target> targets) {
        m_leftTargets.addAll(targets);
    }
    
    void addBottomTargets(ArrayList<Target> targets) {
        m_bottomTargets.addAll(targets);
    }
    
    @Override
    public void moveBy(Point2D.Float offset) {
        super.moveBy(offset);
        m_greenLight.moveBy(offset);
        
        for(TrafficParticipant participant : m_queue)
        {
            participant.moveBy(offset);
        }
    }
    
    void setGreenLightState(Color state) {
        m_greenLight.setState(state);
    }
    
//    public void focus() {
//        m_focused = true;
//    }
//    
//    public void unfocus() {
//        m_focused = false;
//    }
    
    public boolean contains(int x, int y) {
        Point2D.Float position = new Point2D.Float(m_position.x, m_position.y);
        
        switch(this.determineOrientation())
        {
            case UP:
                position.y += m_height;
                break;
                
            case LEFT:
                position.x += m_width;
                break;               
        }
        
        Rectangle2D.Float bounds = new Rectangle2D.Float(position.x, position.y, Math.abs(m_width), Math.abs(m_height));
        
        return bounds.contains(x, y);
    }
}
