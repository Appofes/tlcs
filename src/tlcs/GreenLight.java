/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tlcs;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Point2D;

/**
 *
 * @author Р”РјРёС‚СЂРёР№
 */

public class GreenLight extends GraphicsObject {
    protected Color m_currentState;
    //protected SWITCH_DIRECTION m_switchDirection;
    protected final Color DEFAULT_INITIAL_STATE = Color.RED;
    //protected static float m_greenDelay;
    //protected static float m_yellowDelay;
    //protected static float m_redDelay;
    //protected float m_accumulator;
    /*DETECTOR OF SMTH*/
    
    public GreenLight(Point2D.Float position, int size, int delay, Color initial_state) {
        m_position = new Point2D.Float(position.x, position.y);
        
        m_width = size;
        m_height = size;
        
        //m_delay = (0 < delay) ? delay : DEFAULT_DELAY;
        
        m_currentState = (initial_state.equals(Color.YELLOW)) ? DEFAULT_INITIAL_STATE : initial_state;
    
        //m_switchDirection = (m_currentState.equals(Color.GREEN)) ? SWITCH_DIRECTION.UP : SWITCH_DIRECTION.DOWN;
        
        //m_accumulator = 0.0f;
    }
    
    @Override
    void draw(Graphics2D context) {
        context.setColor(m_currentState);
        context.fillOval((int)m_position.x, (int)m_position.y, m_width, m_height);
    }
    
    @Override
    void update(float delta_time) {
        /*do some update here*/
        //m_accumulator += delta_time;
        //this.switchLight();
    }
    
//    public void switchLight() {
//       if(m_currentState.equals(Color.GREEN) && m_greenDelay <= m_accumulator)
//       {
//            m_currentState = Color.YELLOW;
//            m_switchDirection = SWITCH_DIRECTION.UP;
//            m_accumulator = 0.0f;                      
//       }
//       else if(m_currentState.equals( Color.YELLOW) && m_yellowDelay <= m_accumulator)
//       {
//           m_currentState = (m_switchDirection == SWITCH_DIRECTION.UP) ? Color.RED : Color.GREEN;
//           m_accumulator = 0.0f;
//       }
//       else if(m_currentState.equals(Color.RED) && m_redDelay <= m_accumulator)
//       {
//           m_currentState = Color.YELLOW;
//           m_switchDirection = SWITCH_DIRECTION.DOWN;
//           m_accumulator = 0.0f;
//       }
//    }
    
//    public void setDelay(int delay) {
//        if(0 < delay)
//        {
//            m_delay = delay;
//        }
//    }
//    
//    public int getDelay() {
//        return m_delay;
//    }
    
    public Color getCurrentState() {
        return new Color(m_currentState.getRGB());
    }
    
//    public static void setGreenDelay(float delay) {
//        m_greenDelay = delay;
//    }
//    
//    public static void setYellowDelay(float delay) {
//        m_yellowDelay = delay;
//    }
//    
//    public static void setRedDelay(float delay) {
//        m_redDelay = delay;
//    }
    
    public void setState(Color state) {
        m_currentState = state;
    }
}

enum SWITCH_DIRECTION {UP, DOWN}


