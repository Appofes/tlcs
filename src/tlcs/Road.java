/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tlcs;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

/**
 *
 * @author Дмитрий
 */

public class Road extends GraphicsObject {
    //private final int DEFAULT_LANE_HEIGHT = 100;
    //private final int DEFAULT_LANE_WIDTH = 20;
    private Point2D.Float m_startPosition;
    private ROAD_PLACE m_roadPlace;
    private ArrayList<Lane> m_lanes;
    private int m_actualLaneHeight;
    private int m_actualLaneWidth;
    private Point2D.Float m_offset;
    
    private Zebra m_zebra;

    public Road(ROAD_PLACE roadPlace, Point2D.Float start_position) {
        m_roadPlace = roadPlace;
        m_lanes = new ArrayList<Lane>();
        m_startPosition = new Point2D.Float(start_position.x, start_position.y);
        
        switch(m_roadPlace)
        {
            case TOP:
                m_actualLaneWidth = UI.DEFAULT_LANE_WIDTH;
                m_actualLaneHeight = -UI.DEFAULT_LANE_HEIGHT;
                m_offset = new Point2D.Float(UI.DEFAULT_LANE_WIDTH, 0);
                break;
                
            case BOTTOM:
                m_actualLaneWidth = UI.DEFAULT_LANE_WIDTH;
                m_actualLaneHeight = UI.DEFAULT_LANE_HEIGHT;
                m_offset = new Point2D.Float(UI.DEFAULT_LANE_WIDTH, 0);
                break;
                
            case LEFT:
                m_actualLaneWidth = -UI.DEFAULT_LANE_HEIGHT;
                m_actualLaneHeight = UI.DEFAULT_LANE_WIDTH;
                m_offset = new Point2D.Float(0, UI.DEFAULT_LANE_WIDTH);
                break;
                
            case RIGHT:
                m_actualLaneWidth = UI.DEFAULT_LANE_HEIGHT;
                m_actualLaneHeight = UI.DEFAULT_LANE_WIDTH;
                m_offset = new Point2D.Float(0, UI.DEFAULT_LANE_WIDTH);
                break;
        }
        
        m_zebra = null;
    }
    
//    public void pushLane(Lane lane) {
//        m_lanes.add(lane);
//    }
    
    public void pushLane(boolean to_cross_dir) {
        if(m_lanes.isEmpty())
        {
            m_lanes.add(new Lane(m_startPosition, m_actualLaneWidth, m_actualLaneHeight, to_cross_dir));
//            Lane lane = new Lane(m_startPosition, m_actualLaneWidth, m_actualLaneHeight, to_cross_dir);//
//            lane.setTexture(Lane.DEFAULT_LANE_TEXTURE_PATH);//
//            m_lanes.add(lane);//
        }
        else
        {
            Point2D.Float position = new Point2D.Float();
            Lane lastLane = m_lanes.get(m_lanes.size() - 1);
            
            position.x = lastLane.getPosition().x + m_offset.x;
            position.y = lastLane.getPosition().y + m_offset.y;
            
//            Lane lane = new Lane(position, m_actualLaneWidth, m_actualLaneHeight, to_cross_dir);//
//            lane.setTexture(Lane.DEFAULT_LANE_TEXTURE_PATH);//
//            m_lanes.add(lane);//
            
            m_lanes.add(new Lane(position, m_actualLaneWidth, m_actualLaneHeight, to_cross_dir));
            
//            if(this.hasZebra())
//            {
//                this.pushZebra();
//            }
            //this.adjustZebra();
            if(this.hasZebra())
            {
                m_zebra.moveSecondLaneBy(m_offset);
            }
        }
    }
    
//    public Lane popLane() {
//        if(!m_lanes.isEmpty())
//        {
//            Lane outLane = m_lanes.get(m_lanes.size() - 1);
//            m_lanes.remove(m_lanes.size() - 1);
//            return outLane;
//        }
//        else
//        {
//            return null;
//        }        
//    }
    
    public void popLane() {
        if(!m_lanes.isEmpty())
        {
            m_lanes.remove(m_lanes.size() - 1);
            
//            if(this.hasZebra())
//            {
//                this.pushZebra();
//            }
            //this.adjustZebra();
            if(m_lanes.size() == 0)
            {
                m_zebra = null;
            }
            else if(hasZebra())
            {
                m_zebra.moveSecondLaneBy(new Point2D.Float(-m_offset.x, -m_offset.y));
            }
        }
    }
    
    public void pushZebra() {
        if(this.isEmpty())
        {
            m_zebra = null;
            return;
        }
        
        Point2D.Float firstOrigin = new Point2D.Float();
        Point2D.Float secondOrigin = new Point2D.Float();
        boolean isZebraVertical = true;
        
        Lane firstLane = m_lanes.get(0);
        Lane lastLane = m_lanes.get(m_lanes.size() - 1);
        
        if(m_roadPlace == ROAD_PLACE.TOP || m_roadPlace == ROAD_PLACE.BOTTOM)
        {
            int additionalOffset = (m_roadPlace == ROAD_PLACE.TOP) ? 0 : -m_actualLaneWidth;
            
            firstOrigin.x = firstLane.getPosition().x;
            //firstOrigin.y = firstLane.getPosition().y + m_actualLaneHeight / 2;
            firstOrigin.y = firstLane.getPosition().y + m_actualLaneHeight + additionalOffset; 
            
            secondOrigin.x = lastLane.getPosition().x + m_actualLaneWidth;
            //secondOrigin.y = firstLane.getPosition().y + m_actualLaneHeight / 2; ERROR! LAST LANE, NOT FIRST LANE!
            secondOrigin.y = lastLane.getPosition().y + m_actualLaneHeight + additionalOffset;
            
            isZebraVertical = false;
        }
        else
        {
            int additionalOffset = (m_roadPlace == ROAD_PLACE.LEFT) ? 0 : -m_actualLaneHeight;
            
            firstOrigin.x = firstLane.getPosition().x + m_actualLaneWidth + additionalOffset;
            //firstOrigin.x = firstLane.getPosition().x + m_actualLaneWidth / 2;
            firstOrigin.y = firstLane.getPosition().y;
            
            secondOrigin.x = lastLane.getPosition().x + m_actualLaneWidth + additionalOffset;
            //secondOrigin.x = lastLane.getPosition().x + m_actualLaneWidth / 2;
            secondOrigin.y = lastLane.getPosition().y + m_actualLaneHeight;
            
            isZebraVertical = true;
        }
        
        //CreateZebra
        m_zebra = new Zebra(firstOrigin, secondOrigin, isZebraVertical);
    }
    
    public void popZebra() {
        m_zebra = null;
    }
    
    public boolean hasZebra() {
        return (m_zebra != null);            
    }
    
    public void adjustZebra() {
        if(this.hasZebra())
        {
            this.pushZebra();
        }
    }
    
    public boolean isEmpty() {
        return m_lanes.isEmpty();
    }
    
    public int getNumLanes() {
        return m_lanes.size();
    }
    
    public Color getState() {
        return m_lanes.get(0).getGreenLightState();        
    }
    
    public void setState(Color state) {
        for(Lane lane : m_lanes)
        {
            lane.setGreenLightState(new Color(state.getRGB()));
        }
//        if(this.hasZebra())
//        {
//            if(state.equals(Color.RED))
//            {
//                m_zebra.setState(Color.GREEN);
//            }
//            else
//            {
//                m_zebra.setState(Color.RED);
//            }
//        }
    }
    
    public void setZebraState(Color state) {
        if(this.hasZebra())
        {
            m_zebra.setState(state);
        }
    }

    @Override
    void draw(Graphics2D context) {
        for(Lane lane : m_lanes)
        {
            lane.draw(context);
        }
        if(m_zebra != null)
        {
            m_zebra.draw(context);
        }
    }

    @Override
    void update(float delta_time) {
        for(Lane lane : m_lanes)
        {
            lane.update(delta_time);
        }
    }
    
    @Override
    public void moveBy(Point2D.Float offset) {
        if(!m_lanes.isEmpty())
        {
            for(Lane lane : m_lanes)
            {
                lane.moveBy(offset);
            }
        }
        
        m_startPosition.x += offset.x;
        m_startPosition.y += offset.y;
        
//        if(this.hasZebra())
//        {
//            this.pushZebra();
//        }
        //this.adjustZebra();
        if(this.hasZebra())
        {
            m_zebra.moveAllBy(offset);
        }
    }
    
//    public Rectangle2D getBounds() {
//        if(!m_lanes.isEmpty())
//        {
//            
//        }       
//    }
    
    public Lane getFocusedLane(int x, int y) {
        for(Lane lane : m_lanes)
        {
            if(lane.contains(x, y))
            {
                return lane;
            }
        }          
        
        if(this.hasZebra())
        {
            return m_zebra.getFocusedLane(x, y);
        }
        
        return null;
    }
}